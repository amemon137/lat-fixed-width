import random

from testutils import *
from code import *


# Task 1: Generate a fixed width file using the given spec
spec = load_spec_file()
file = FixedWidthFile(spec, 'file.txt')

for i in range(10):
    row = [rand_str(random.randint(0, width)) for width in spec['Offsets']]
    file.add(row)

file.save()


# Task 2: Implement a parser that can parse the fixed width file and generate
# a delimited file
existing_file = FixedWidthFile(spec, 'file.txt')

existing_file.load()

existing_file.save_to_csv_file('answer.csv')
