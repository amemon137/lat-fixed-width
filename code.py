import csv


empty_char = ' '
special_chars_escape_map = {
    ' ': '\\s',
    '\\': r'\\'
}
new_line = '\n'
forbidden_chars = ['\n']


def escape_str(str_val: str):
    return str_val.translate(str_val.maketrans(special_chars_escape_map))


def unescape_str(str_val: str):
    # TODO
    return str_val


def create_cell_str(width: int, content: str):
    original_len = len(content)

    for char in forbidden_chars:
        if char in content:
            raise ForbiddenCharacterError(char, content)

    if original_len > width:
        raise CellOverflowError(width, content)

    escaped_content = escape_str(content)
    pad_length = len(escaped_content) - original_len + width
    padded_content = escaped_content.ljust(pad_length, empty_char)

    return padded_content


def create_row_str(offsets: list, content: str):
    if len(content) != len(offsets):
        raise RowSizeMismatch(len(offsets), content)

    row_str = ''
    for i in range(0, len(offsets)):
        row_str += create_cell_str(offsets[i], content[i])

    return row_str


def extract_row(offsets: list, row_str: str):
    row = []
    current = 0

    for offset in offsets:
        cell = unescape_str(row_str[current:(current + offset)].strip())

        if (len(cell) > offset):
            raise CellOverflowError(offset, cell)

        row.append(cell)
        current += offset

    return row


class CellOverflowError(Exception):
    def __init__(self, width: int, content: str):
        self.width = width
        self.content = content

    def __str__(self):
        return "Cell overflow. ({}) {}".format(self.width, self.content)


class ForbiddenCharacterError(Exception):
    def __init__(self, char: str, content: int):
        self.char = char
        self.content = content

    def __str__(self):
        return "Forbidden character. ({}) {}".format(self.char, self.content)


class RowSizeMismatchError(Exception):
    def __init__(self, expected_cols: list, row: list):
        self.row = row
        self.expected_cols = expected_cols

    def __str__(self):
        return "Row size mismatch. Expected cols: {}, Actual: {}".format(
            self.expected_cols, len(self.row)
        )


class FixedWidthFile():
    def __init__(self, spec: dict, file_path: str):
        self.file_path = file_path
        self.spec = spec
        self.rows = []

    def load(self):
        with open(
            self.file_path, 'r', encoding=self.spec['FixedWidthEncoding']
        ) as f:
            if self.spec['IncludeHeader']:
                header_row_str = f.readline()

            for row_str in f:
                self.rows.append(extract_row(self.spec['Offsets'], row_str))

    def get_rows(self):
        return self.rows

    def save(self):
        with open(
            self.file_path, 'w', encoding=self.spec['FixedWidthEncoding']
        ) as f:
            if self.spec['IncludeHeader']:
                header_row_str = create_row_str(
                    self.spec['Offsets'],
                    self.spec['ColumnNames']
                )
                f.write(header_row_str + new_line)

            # not using list comprehension because it makes things very cryptic
            row_strs = []
            for row in self.rows:
                row_strs.append(create_row_str(self.spec['Offsets'], row))

            f.write(new_line.join(row_strs))

    def add(self, row: list):
        if len(row) != len(self.spec['Offsets']):
            raise RowSizeMismatchError(len(self.spec['Offsets']), row)

        self.rows.append(row)

    def save_to_csv_file(self, path: str):
        with open(
            path, 'w', newline='', encoding=self.spec['DelimitedEncoding']
        ) as csvfile:
            writer = csv.writer(csvfile, delimiter=',')
            writer.writerow(self.spec['ColumnNames'])

            for row in self.rows:
                writer.writerow(row)
