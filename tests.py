import os
import random
import unittest

from code import (
    escape_str,
    unescape_str,
    extract_row,
    create_cell_str,
    create_row_str,
    CellOverflowError,
    FixedWidthFile,
    RowSizeMismatchError
)
from testutils import get_file_lines, load_spec_file, rand_str


test_file_path = 'test_file'
test_existing_file_path = 'test_existing_file.txt'


class TestFixedWidthFile(unittest.TestCase):

    def setUp(self):
        if os.path.exists(test_file_path):
            os.remove(test_file_path)

    def test_save_creates_new_file(self):
        spec = load_spec_file()
        file = FixedWidthFile(spec, test_file_path)

        file.save()

        self.assertTrue(os.path.exists(test_file_path))

    def test_create_row_str_returns_row_with_correct_content_and_widths(self):
        actual_row = create_row_str([3, 4, 1], ['a', 'defe', ''])
        expected_row = 'a  defe '

        self.assertEqual(expected_row, actual_row)

    def test_save_adds_header_with_correct_titles_and_widths(self):
        spec = load_spec_file()
        offsets = spec['Offsets']
        column_names = spec['ColumnNames']
        expected_header = ''.join([
            col[0].ljust(col[1], ' ') for col in zip(column_names, offsets)
        ])

        file = FixedWidthFile(spec, test_file_path)

        file.save()

        actual_header = get_file_lines(test_file_path)[0]

        self.assertEqual(expected_header, actual_header)

    def test_create_cell_str_returns_correct_content_and_width(self):
        actual_val = create_cell_str(4, 'abc')
        expected_val = 'abc '

        self.assertEqual(expected_val, actual_val)

    def test_create_cell_str_should_escape_space(self):
        actual_val = create_cell_str(4, 'ab ')
        expected_val = r'ab\s '

        self.assertEqual(expected_val, actual_val)

    def test_escape_str_should_escape_special_chars(self):
        str = r' f e r\ '
        actual_val = escape_str(str)
        expected_val = r'\sf\se\sr\\\s'

        self.assertEqual(expected_val, actual_val)

    def test_create_cell_str_should_throw_overflow_exception(self):
        self.assertRaises(CellOverflowError, create_cell_str, 4, 'ab   ')

    def test_add_and_save_should_save_the_added_row(self):
        spec = load_spec_file()
        file = FixedWidthFile(spec, test_file_path)
        content = ['a'] * len(spec['Offsets'])

        file.add(content)
        file.save()

        contents = get_file_lines(test_file_path)

        self.assertEqual(create_row_str(spec['Offsets'], content), contents[1])

    def test_add_should_throw_invalid_size_mismatch_if_row_size_does_not_match_cols(self):
        spec = load_spec_file()
        file = FixedWidthFile(spec, test_file_path)
        content = ['a'] * (len(spec['Offsets']) + 1)

        self.assertRaises(RowSizeMismatchError, file.add, content)

    def test_extract_row_should_return_correct_values(self):
        # |a  |b |c c |
        row = extract_row([3, 2, 4], 'a  b c c ')

        self.assertListEqual(['a', 'b', 'c c'], row)

    def test_load_add_save_file_should_contain_new_row(self):
        spec = load_spec_file()
        file = FixedWidthFile(spec, test_file_path)
        row = [rand_str(random.randint(0, width)) for width in spec['Offsets']]
        file.add(row)
        file.save()

        existing_file = FixedWidthFile(spec, test_file_path)
        existing_file.load()
        rows = existing_file.get_rows()

        self.assertListEqual(rows[0], row)

    def test_save_to_csv_file_should_save_correctly(self):
        spec = load_spec_file()
        file = FixedWidthFile(spec, test_file_path)
        row = [rand_str(random.randint(0, width)) for width in spec['Offsets']]
        file.add(row)
        file.save_to_csv_file('test.csv')

        lines = get_file_lines('test.csv')

        self.assertEqual(','.join(row), lines[1])


if __name__ == '__main__':
    unittest.main()
