import json
import random


def load_spec_file(path='spec.json'):
    f = open(path, 'r')
    contents = f.read()
    f.close()

    json_obj = json.loads(contents)
    json_obj['Offsets'] = [int(offset) for offset in json_obj['Offsets']]

    json_obj['IncludeHeader'] = (json_obj['IncludeHeader'] == 'True')

    return json_obj


def get_file_lines(path):
    f = open(path, 'r')
    lines = f.read().splitlines()
    f.close()

    return lines


def rand_str(length, chars=['a', 'b', 'c']):
    return ''.join([random.choice(chars) for i in range(0, length)])
